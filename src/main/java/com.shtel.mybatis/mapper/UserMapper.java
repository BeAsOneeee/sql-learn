import com.shtel.mybatis.entity.User;

import java.util.List;

public interface  UserMapper{
    void insert(User user);
    void update(User user) throws Exception;
    void delete(Long id) throws Exception;
    User selectById(Long id) throws  Exception;
    List<User> selectAll() throws Exception;
}