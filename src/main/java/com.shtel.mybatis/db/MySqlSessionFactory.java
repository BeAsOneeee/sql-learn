import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import java.io.Reader;

public class MySqlSessionFactory{
    public static SqlSessionFactory sessionFactory;


    private static final Resources Resource = null;

    static {
        try{
            Reader reader =Resource.getResourceAsReader("mybatis-config.xml");
            sessionFactory=new SqlSessionFactoryBuilder().build(reader,"dev");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public static SqlSession getSession(){
        return sessionFactory.openSession(true);
    }
}