package com.shtel.mybatis.service;

import com.shtel.mybatis.db.MySqlSessionFactory;
import com.shtel.mybatis.entity.User;
import com.shtel.mybatis.mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class UserServiceImpl implements UserService{
    @Override
    public void insert(User user) throws Exception{
        SqlSession session =MySqlSessionFactory.getSession();
        UserMapper mapper=session.getMapper(UserMapper.class);
        try{
            mapper.insert(user);
        }finally{
            session.close();
            }
    }
    @Override
    public void update(User user) throws Exception{
        SqlSession session=MySqlSessionFactory.getSession();
        UserMapper mapper=session.getMapper(UserMapper.class);
        try{
            mapper.update(user);
        }finally{
            session.close();
            }
    }
    @Override
    public void delete(Long id) throws Exception{
        SqlSession session =MySqlSessionFactory.getSession();
        UserMapper mapper=session.getMapper(UserMapper.class);
        try{
            mapper.delete(id);
            }finally{
            session.close();
            }
        }

    @Override
    public User selectById(Long id) throws Exception{
        User user=null;
        SqlSession session=MySqlSessionFactory.getSession();
        UserMapper mapper=session.getMapper(UserMapper.class);
        try{
            user=mapper.selectById(id);
        }finally{
            session.close();
            }
           return user;
        }

}